package tests;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import utils.Paths;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class ConsultaCepTest {

    int contador = 0;

    @Test
    public void deveRetornarDadosEnderecoComCepValido(){
/home/qa/projetos/prova_sicredi_api
        given().
        when()
                .get(Paths.PATH_CEP_VALIDO)
        .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/CasoSucessoSCHEMA.json"))
                .log()
                .all()
                .extract().response().path("logradouro", "Avenida Assis Brasil 3940");
    }

    //TODO: Caso é passado o cep "00000000" que não consta cadastro nos correios. A Api retornar sucesso nesse caso, mesmo sendo inexistente.
    @Test
    public void deveRetornarDadosEnderecoComCepInexistente(){

        given().
        when()
                .get(Paths.PATH_CEP_INEXISTENTE)
        .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/ErroSCHEMA.json"))
                .log()
                .all()
                .extract().response().path("erro", String.valueOf(equalTo(true)));
    }

    @Test
    public void deveRetornarDadosEnderecoComCepInvalido(){


        given().
        when()
                .get(Paths.PATH_CEP_INVALIDO)
        .then()
                .assertThat()
                .contentType(ContentType.HTML)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(containsString("Verifique a sua URL (Bad Request)"),
                        containsString("ViaCEP 400"))
                .log()
                .all();
    }

    @Test
    public void deveRetornarDadosEnderecoComCepCaracteresEspeciais(){


        given().
        when()
                .get(Paths.PATH_CEP_CARACTERES_ESPECIAIS)
        .then()
                .assertThat()
                .contentType(ContentType.HTML)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(containsString("Verifique a sua URL (Bad Request)"),
                        containsString("ViaCEP 400"))
                .log()
                .all();
    }

    @Test
    public void deveRetornarCEPsDeRuasComAlgumNomeEmComum(){

    List<Object> listaEnderecos = given().
        when()
                .get(Paths.PATH_CEP_RUAS_NOMES_EM_COMUM)
        .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/RuasNomeEmComumSucessoSCHEMA.json"))
                .log()
                .all()
                .extract().jsonPath().getList("");

        for(Object endereco : listaEnderecos){
            assertEquals(endereco, listaEnderecos.get(contador));
            contador++;
        }

    }
}
