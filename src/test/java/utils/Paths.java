package utils;

public class Paths {

    public static final String PATH_CEP_VALIDO = "https://viacep.com.br/ws/91060900/json/";
    public static final String PATH_CEP_INEXISTENTE = "https://viacep.com.br/ws/00000000/json/";
    public static final String PATH_CEP_INVALIDO = "https://viacep.com.br/ws/9106/json/";
    public static final String PATH_CEP_CARACTERES_ESPECIAIS = "https://viacep.com.br/ws/%&$@#/json/";
    public static final String PATH_CEP_RUAS_NOMES_EM_COMUM = "https://viacep.com.br/ws/RS/Gravatai/Barroso/json/";
}
